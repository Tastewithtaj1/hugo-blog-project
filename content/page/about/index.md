---
title: About
description: Taste with TJ
date: '2019-02-28'
aliases:
  - about-us
  - about-hugo
  - contact
license: CC BY-NC-ND
lastmod: '2020-10-09'
menu:
    main: 
        weight: -90
        params:
            icon: user
---

Salam / Hi everyone this is Taste with TJ is here to help you taste, best homemade food. 

Our focus is to bring food with diversified tasty recipes. Those who have a sense of taste will automatically become part of our Family Inshallah.  Let's be Partners in Taste.

We will cover all known tastes but obviously your taste buds will sense a flavor of our local Pakistan food tradition.  
Hopefully you will see us as your daily friend on youtube with a lot of yummy love recipes and delicious smiles. 

We have planned to schedule our uploads this way 

Weekly Recipe 1 -2 

See you then buddies 

Team Taste with TJ
Hafsa Taj and Umair Taj

NOTE: Taste with TJ content is copyright protected and must not be used without written permission.

