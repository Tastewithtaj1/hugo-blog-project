---
title: "Chicken BBQ ( Barbecue ) Recipe"
description: 
date: 2022-03-26T17:33:07+05:00
image: https://res.cloudinary.com/taste-with-taj/image/upload/v1644132794/Blog-Assets%20TwT/Easy%20bbq%20Chicken%20Recipe/header_image_k5rblo.jpg
categories:
    - Chicken Recipes

tags:
    -
    - 
    - 
math: 
license: 
hidden: false
comments: true
draft: false
url: "/chicken-bbq-barbecue-recipe/"
---

{{< youtube m3ou39ba_SE >}}

This Chicken BBQ Recipe is the ideal chicken leg recipe since it doesn't need any elaborate marination or spices. It's all about this simple cooking process that doesn't require an oven. This may be sweetened with BBQ sauce for moist and juicy grilled chicken every time, but I used Chicken Tikka Masala instead during the marination step. BBQ chicken, whether grilled or baked in the oven, is one of those meals that no one at the table ever complains about. As long as  it's dry and overdone. Don't worry mine is fine as you can see in the video, it's juicy BBQ. It's a simple technique that anybody can learn so that they, too, can say that their BBQ chicken is the finest in town. I'd tasted many chicken BBQ near me around home and figured why not cook my own? So gather the following ingredients to begin the Chicken BBQ at home Recipe.


## Ingredients for chicken barbecue

- CHICKEN 4 PIECES

- YOGURT ½ CUP

- GINGER GARLIC PASTE 1 TBSP

- CHICKEN TIKKA MASALA 3 TBSP

- LEMON JUICE 1 LEMON

- OIL/ DESI GHEE AS REQUIRED
 
## Directions to Cook BBQ Chicken

- Cuts on Chicken Leg and Thigh Piece:

Grab a chicken leg and thigh piece and begin cutting deep cuts on both sides to allow the marination to settle for the    greatest taste.
 
-  Marination Flavors:

Simply combine 1/2 cup of yoghurt, 1 tablespoon of ginger garlic paste, and 1 lemon juice. 3 tbsp Chicken Tikka Masala, properly combined
 
- Marination Time:

If you want a true taste in every mouthful of chicken BBQ, marinate the chicken overnight. You can always add BBQ sauce to the mix later, but I used Chicken Tikka Masala for this step.

- Grill the chicken pieces:

It's time to cross the BBQ rods from the chicken pieces and set them on the grill. Continue to flip them from all sides so that  they cook evenly from the inside out. I recommend cooking the chicken pieces over medium heat since high heat will cause the outside to cook while leaving the inside uncooked. So the only skill that will create taste is cooking it correctly without burning it.


 Alhamdulillah, it's finished!

  
### Common Thoughts or Concerns in making BBQ Chicken

- Should we use boneless chicken breasts or chicken with bones?

 The flavour of boneless chicken breasts will be less than that of bone-in. That's why basting with BBQ sauce towards the end of the cooking time adds a lot of flavors. And, when properly seasoned and cooked, even my simple recipe contains garlic paste.
 The white flesh of chicken breasts is preferred by the majority of chicken eaters. So, for quick-cooking chicken, I use basic, skinless, bone-in chicken legs. Of course, you may use the same cooking process for skin-on, boneless chicken breasts or thighs. The procedure remains the same, however the cooking periods will differ.

- Is it better to eat skinless chicken or skin on chicken?

 When it comes to chicken barbecue, it doesn't matter what kind of chicken you use. I choose skinless chicken for BBQ because it cooks quickly and my taste is more developed with skinless chicken. That is for sure a matter of personal choice.

- Is it true that using chicken at Grilled with fire or coal makes a difference?

 Yes, it has a significant influence on taste. If you put coal in your grill, it will have a distinct flavor and aroma that many people like. On this recipe, I did not use any coal in the grill. If you've seen, I'm using a BBQ stove that was expressly designed for both uses; I can use coals at the base or a gas alternative. And this time I went with the gas option.

- Another question is if an oven or grill is a possibility?

 Yes, it is totally dependent on the equipment available. Please keep in mind that your chicken BBQ taste may vary depending on these parameters, and that experimenting with various methods will introduce you to new things. I've used the oven in a variety of chicken dishes, including my [Chicken Drumsticks in Oven Recipe](), which has a distinct taste.


## Sign Up and be our Partners in Taste
Please [Subscribe to our YouTube Channel]() if you liked this Chicken BBQ Recipe Also Click the BELL icon so you can be the first one to get notification of Taste with Taj new video). Thank you for signing up. It means a lot to me.

## Filming and Post Processing :

[Explore with Taj](https://www.youtube.com/c/ExplorewithTaj/videos)


#tastewithtaj  #Chicken    #Barbecue     #ChickenBarbecue    #ChickenBarbecueRecipe


## Gallery pictures

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644132794/Blog-Assets%20TwT/Easy%20bbq%20Chicken%20Recipe/Gallery_image_3_hzjj6d.jpg)  ![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644132794/Blog-Assets%20TwT/Easy%20bbq%20Chicken%20Recipe/Gallery_image_1_vmzo1k.jpg)   ![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644132794/Blog-Assets%20TwT/Easy%20bbq%20Chicken%20Recipe/Gallery_image_2_ppsnho.jpg)  


