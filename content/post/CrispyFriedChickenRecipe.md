---
title: "Fried Chicken Recipe"
description: 
date: 2022-03-25T17:31:07+05:00
image: https://res.cloudinary.com/taste-with-taj/image/upload/v1644143974/Blog-Assets%20TwT/Crispy%20Fried%20Chicken%20Recipe/Gallery_Image_3_vurkch.jpg
categories:
    - Chicken Recipes

tags:
    - 
math: 
license: 
hidden: false
comments: true
draft: false
url: "/crispy-fried-chicken-recipe/"
---

{{< youtube 5CdntkJfqBc >}}

# Fried Chicken Recipe

Today, let's cook a crispy fried chicken recipe together. Taste with Taj enjoys this recipe for making it at home in the style of KFC. This fried chicken recipe is also well-known. I'll walk you through the whole process of creating fried chicken from start to finish, with the crispiest, most delicious crispy exterior and moist and juicy flesh on the inside. In this recipe, I used chicken drumsticks.
 
When it comes to making chicken recipes, there is nothing more amazing than a juicy, crispy piece of finger-licking fantastic fried chicken. It may seem overwhelming to fry your own chicken, but it's really rather simple and much superior than grocery store and fast restaurant fried chicken.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644143974/Blog-Assets%20TwT/Crispy%20Fried%20Chicken%20Recipe/Gallery_Image_1_xwyf3f.jpg)
 
## Fried Chicken Recipe Ingredients ( A Crispy one )

### For Marination 

✅ 1 ½  Cup of MILK 

✅ 1 ½  Tbsp WHITE VINEGAR

✅ 3 Tsp or As Required SALT

✅ 1 Tsp DRY ROSEMARY 

✅ 1 ½ Tsp RED CHILI POWDER

✅ 1 Tsp FRIED CHICKEN MASALA 

✅ 1 Tbsp GARLIC PASTE

✅ 1 Tbsp GINGER PASTE

✅ 3 EGGs

✅ 1 Kg CHICKEN DRUMSTICKS 

### For Dry Ingredients

✅ 1 ½ ALL PURPOSE FLOUR

✅ 1 Cup CORN STARCH 

✅ 1 ½ Tsp or As Required SALT

✅ 1 Tsp RED CHILI POWDER

✅ 1 Tsp CORIANDER POWDER

✅ 1 Tsp BLACK PEPPER POWDER

✅ 1 Tsp BAKING POWDER

✅ 1 Tsp GARLIC POWDER 

✅ 1 Tsp FENNEL SEEDS

✅ 1 Tsp FRIED CHICKEN MASALA 

✅ OIL for Deep Fry
 
## Directions of making Yummy Fried Chicken Recipe
 
### Procedure for Marinating Chicken
 
Pour 1 1/2 cups of milk into a bowl and add 1 1/2 tablespoons of white vinegar. Allow it to sit for 5 minutes to create butter milk.
Before adding any other ingredients, give it a quick toss. Add 3 tsp (or as needed) salt, 1 tsp dried rosemary, and 1 1/2 tbsp red chilli powder. Continue to combine it.

The main ingredient of fried chicken, 1 teaspoon masala fried chickenI'm adding garlic paste to take the flavour to the next level.
Mix in 3 eggs well.

It's time to dip the chicken pieces (I used chicken drum sticks for this recipe) in the liquid we produced in the previous stages.
Marinate the chicken for at least 5–6 hours. The longer you marinate the fried chicken, the more juicy and delicious it will be.
 
### Breading and marinating chicken pieces
 
At this point, we need to create some substance for breading chickens.
 
I recommend using a flat-bottomed container so that we have enough area to flour the chicken.
 
Add 1 1/2 cups all-purpose flour and mix well. 1 tsp Red Chili Powder, 12 tsp Or As Needed Salt, 1 tsp Coriander Powder, 1 tsp Black Pepper Powder, 1 tsp Fried Chicken Masala, 1 tsp Fennel Seeds, 1 tsp Garlic Powder, 1 tsp Baking Powder, 1 cup Corn Starch
![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1649843602/Blog-Assets%20TwT/Crispy%20Fried%20Chicken%20Recipe/Snapshot_4_owywgi.jpg) 
Remove the chicken pieces (drum sticks) one at a time from the buttermilk mixture. Shake it carefully to get rid of the excess. Coat it completely in the breading mixture. Any excess should be tapped off.
 
### The Last Steps of the Fried Chicken Recipe
 
Make sure the oil is heated slowly before adding the chicken pieces. 3–4 pieces may be fried together. Deep frying requires at least 4 inches of oil. This frying will take 13 to 15 minutes, but keep a watch on each piece to ensure it does not overcook.
 
The top butter coating will be done instantly, but the goal is to cook the chicken from the inside out over low heat.
 
Alhamdulillah, the fried chicken is ready and thoroughly prepared from the inside. Simply set it aside for 10 minutes before serving. I hope you enjoy the meal.
 ![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1649843602/Blog-Assets%20TwT/Crispy%20Fried%20Chicken%20Recipe/Snapshot_5_eelbie.jpg) 
## A Few Frequently Asked Questions:
 
### Can I substitute cornstarch for all-purpose flour?
 
To be honest, this is the primary component in the crispiest fried chicken. So I recommend a mix of all-purpose flour and corn starch for the best crispy outcomes. However, you may substitute cornstarch for all-purpose flour or use gluten-free flour.
 
### How long should I cook chicken for?
 
Stick to 3–4 pieces at a time and cook for 13–15 minutes. Again, I believe you should keep a close eye on each piece to prevent overcooking.
 
### Is it better to use fresh or re-used frying oil?
 
Because this fried chicken recipe has a distinct flavor, it is recommended to use a fresh batch of oil; otherwise, the taste of earlier frying may linger, which I particularly dislike. Yes, this oil may be used in any conventional cooking recipe later on. For frying, most people use vegetable oil, although there are other possibilities, such as canola oil and corn oil.
 
### Is this recipe exclusively for chicken drumsticks?
 
No, you may make the chest piece using the same recipe. We chose this since we all like chicken drumsticks at home. The same procedure applies to whatever chicken you use, even boneless chicken.

### Sign Up and be our Partners in Taste
Please [Subscribe to our YouTube Channel]() if you liked this Fried Chicken Recipe Also Click the BELL icon so you can be the first one to get notification of Taste with Taj new video). Thank you for signing up. It means a lot to me.

#tastewithtaj  #KFCstyleFriedChickenRecipe #Crispychicken

Learn FRIED CHICKEN  | KFC style Fried Chicken Recipe |  Spicy Crispy chicken fry by  Taste with Taj

## Filming and Post Processing :

 [Explore with Taj](https://www.youtube.com/c/ExplorewithTaj/videos)

## Gallery pictures

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644143974/Blog-Assets%20TwT/Crispy%20Fried%20Chicken%20Recipe/Gallery_Image_2_oss2kq.jpg)   ![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644143974/Blog-Assets%20TwT/Crispy%20Fried%20Chicken%20Recipe/Header_Image_vft9gs.jpg)   ![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644143974/Blog-Assets%20TwT/Crispy%20Fried%20Chicken%20Recipe/Gallery_Image_4_wep1wn.jpg)
 

