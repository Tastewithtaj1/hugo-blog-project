---
title: "Dessert Recipe Banoffee Pie"
description: 
date: 2022-04-06T17:31:07+05:00
image: https://res.cloudinary.com/taste-with-taj/image/upload/v1644151861/Blog-Assets%20TwT/Good%20Dessert%20Banoffee%20Pie%20Recipe%20No%20Bake/Gallery_image_2_eiunrf.jpg
categories:
    - Desserts
    - Cake Shelf

tags:
    - Dessert Banoffee Pie Recipe No Bake
    - Dessert Recipe at homea
    - Recipe of Dessert Banoffee Pie  No Bake
math: 
license: 
hidden: false
comments: true
draft: false
url: "/dessert-banoffee-pie-recipe-no-bake/"
---

{{< youtube FBwhaGiy_ms  >}}


# Dessert Recipe Banoffee Pie

Before preparing banoffee pie, I tasted a few dessert near me that were similar to banoffee pie, so I decided to make it myself. Please browse for the Dessert playlist or category on our YouTube channel and blog for a variety of delicious dessert recipes.

To be honest, banoffee pie is not a simple dessert to make, so please bear with me. But once it's done, it'll brighten your day, inshallah.

You'll adore this delicious No Bake Banoffee Pie. It's the ultimate dessert, with a wheat cookie crust, condensed milk, fresh bananas, whipped cream, and a hidden layer of dry sweet and dulce de leche. Furthermore, there is no need to turn on the oven!

With its rich, thick condensed milk and huge delicious cream, this British-inspired dessert is definitely a delight. It's also a no-bake recipe, which is ideal for when you have company coming over and want to make dessert ahead of time.

## Ingredients for Dessert Recipe Banoffee Pie

🍰 600 grams Of Wheat Biscuits 

🍰 80 grams Of Butter (Melted)

🍰 200 grams  Of Condensed Milk

🍰 200 grams Of Whipping Cream 

🍰 1 Tbsp Dulce De Leche

🍰 2 Packs Of Gachak

🍰 3-4 Bananas 


## Directions to make a Dessert Recipe Banoffee Pie

### How to make a buttery cookie crust:

Grind wheat biscuits in a food processor until they are powdery. Now, take 80 gram of melted butter and spread it over the crushed wheat biscuits. Blend it well with your hands, and use your fingers to fully mix it.

 ![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644151861/Blog-Assets%20TwT/Good%20Dessert%20Banoffee%20Pie%20Recipe%20No%20Bake/Gallery_image_3_joe6oa.jpg)   

Now, take 80 gram of melted butter and spread it over the crushed wheat biscuits  Blend Mix this well with your hands, and use your fingers to fully mix it.

Spread out on a one-of-a-kind pie tray (the border of which may be detached from the base), keeping the crust intact. Make the layer as thick as possible and place it in the refrigerator for 30 minutes.

### Cooking condensed milk to produce Dulce de Leche:

I purposefully chose condensed milk, which comes in a tin and can be boiled in water. Keep the tin of condensed milk submerged in boiling water for at least 4 hours. Alternatively, for a fast workout, use a pressure cooker for 30 minutes. When it's done, it's Dulce de Leche.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644151861/Blog-Assets%20TwT/Good%20Dessert%20Banoffee%20Pie%20Recipe%20No%20Bake/Gallery_image_1_etj42w.jpg)   

### Creating the Cream Layer:

In a mixing dish, combine 200 g of whipped cream and 200 g of condensed milk. The more you beat the mixture with an egg beater, the softer it will be.

### Toppings :

For the topping, I'll smash two packets of dried sweets for a crispy texture. Second, bananas, I'll be slicing them into circular shapes for aesthetic purposes.

Place dry sweet crunches and banana slices on top of the chilled wheat biscuit crust. Make a wall out of bananas, leaving no gaps on the sides, then fill the centre with them.

At this point, pour condensed milk over the bananas and top with dried sweets.

Finally, add the cream layer that we produced previously and place it in the refrigerator for 3-4 hours.

### Final Form:

Now the moment of truth,  the pie is definitely frozen, and you can top it with coco powder and some extra dry sweet crunch to close the deal.


Alhamdulillah, the dessert recipe for Banoffee Pie is complete. As I previously said, this is not a simple dessert recipe to create, but it is well worth the effort.

There are several dessert ideas that may be adapted with little changes in these no bake pie recipes.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644151861/Blog-Assets%20TwT/Good%20Dessert%20Banoffee%20Pie%20Recipe%20No%20Bake/Header_Image_nkepxp.jpg) 

## Frequently Asked Questions About the Banoffee Pie Dessert Recipe:



### What kind of cookies should I use for the crust?

I made it with wholewheat cookies available . You may use graham crackers (in the United States) or digestive biscuits (UK and Australia). 

### What can I substitute for the sweetened condensed milk?

For the greatest results, use ordinary sweetened condensed milk only.

### Is it possible to prepare the pie ahead of time?

Yes! Make the cookie crust and chill it for a couple of days before serving. Just before serving, top with fresh bananas, whipped cream, and coco powder.Refrigerate any leftovers.

### Sign Up and be our Partners in Taste
Please [Subscribe to our YouTube Channel]() if you liked this Dessert Recipe Banoffee Pie Also Click the BELL icon so you can be the first one to get notification of Taste with Taj new video). Thank you for signing up. It means a lot to me.

## Filming and Post Processing :

 [Explore with Taj](https://www.youtube.com/c/ExplorewithTaj/videos)


 


