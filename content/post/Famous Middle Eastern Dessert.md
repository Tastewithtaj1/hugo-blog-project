---
title: "Famous Middle Eastern Dessert Basbosa  "
description: 
date: 2022-06-29T17:33:07+05:00
image: https://res.cloudinary.com/taste-with-taj/image/upload/v1656603384/Blog-Assets%20TwT/Famous%20Middle%20Eastern%20Dessert%20Basbosa/header_mtlp3a.jpg

categories:
    - Desserts
    
tags:
    - 
math: 
license: 
hidden: false
comments: true
draft: false
url: "/Famous-Middle-Eastern-Dessert-Basbosa/"
---

{{< youtube lGql_bi5Nmk  >}}

## Ingredients

Semolina / Wheatlets 1 Cup

Butter    ½ Cup

Sugar    1 Cup

All Purpose Flour  1 Cup

Coconut   1 Cup

Egg    1 

Baking Powder  1 Tsp

Milk     As Required 

Almonds   15 -20

For Syrup

Sugar    1 Cup

Water    ½ Cup

Lemon    Few Drops

## Method

Put A Pan On Medium Heat, Add Water, Sugar And Lemon Drops

Make Sugar Syrup, Not Too Thick Not Too Thin

Keep Syrup Aside And Make It Lukewarm Syrup 

Take A Bowl Add Egg, Butter And 2 Tbsp Of Milk And Mix Well

Take A Large Bowl, Add Flour, Wheatlets, Sugar, Coconut And Add Wet Ingredients Then Mix Well
Gently Add Milk To Make A Thick Batter, Pre Heat Oven For 15 Minutes On 200 Degrees 

Greece Baking Pan With Oil Add Batter In It And Bake For 35 – 40 Mins On 180 Degrees Until Cake Becomes Brown 

Take Few Almonds, Boil Them And Peel Off Their Skin

Cut Basbosa In Small Cubes And Place Almonds On Per Cube

Add Sugar Syrup Basbosa And Rest It For 10 Minutes Until Sugar Syrup Absorbs In 

It’s Ready Alhamdulillah

## Sign Up and be our Partners in Taste
Please [Subscribe to our YouTube Channel]() if you liked this Beef Burger recipe Also Click the BELL icon so you can be the first one to get notification of Taste with Taj new video). Thank you for signing up. It means a lot to me.


#tastewithtaj  #Breakfast #eggs #How To

## Filming and Post Processing :

 [Explore with Taj](https://www.youtube.com/c/ExplorewithTaj/videos)

## Gallery pictures

 
![Photo by Explore with Taj]()




