---
title: "Break Fast Traditional Spanish Omelet  (Tortilla Espanola)"
description: 
date: 2023-01-12T17:33:07+05:00
image: https://res.cloudinary.com/taste-with-taj/image/upload/v1673716275/Blog-Assets%20TwT/Spanish%20Omelet/1_mlavbe.jpg
categories:
    - 
    - Break Fast

tags:
    - 
math: 
license: 
hidden: false
comments: true
draft: false
url: "/traditional-spanish-omelet-break-fast/"
---

{{< youtube fmn9u3cpyvs  >}}

# Break Fast Traditional Spanish Omelet  (Tortilla Espanola) By Taste With Tj

This popular Spanish Omelet (Tortilla) recipe combines crispy, fried potatoes and eggs, making it ideal for picnics, parties, bbq's, or your traditional Tapas menu! Improve your omelet!

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1673716275/Blog-Assets%20TwT/Spanish%20Omelet/7_zpgnam.jpg)

Spanish omelet is a traditional Spanish dish that can be found at any reputable supermarket. You may serve it warm or cold and take it to a picnic, family gathering, or Spanish-themed party.

## To prepare this wonderful TRADITIONAL SPANISH omelet, we'll need the following components.

To clarify, the Spaniards call it Tortilla Espanola or Tortilla de Patatas. Many of them object to calling this dish an omelet. However, the rest of the world knows it by that name, so I'll use it.

Spanish omelet (Spanish tortilla) is delicious hot or cold and very simple to make! But personally I like it hot and freshly prepared when served.

POTATOES  2 MEDIUM

ONION   1 MEDIUM

SALT   5 PINCHES OR AS REQUIRED

EGG   5

OIL   FOR FRY

## How to Make a Spanish Omelet at home (Tortilla Espaola)

This meal can be cooked ahead of time because it stays well in the fridge and can be eaten cold. It can also be used as an element in sandwiches by sandwiching a slice of the omelet between two slices of bread. Nothing beats a piece of tortilla with a steaming cup of café con leche for a mid-morning snack.



The first step is to prepare the potatoes. 2 medium potatoes should be rinsed, peeled, and sliced. I sliced them in a circular form, but if you want to cut them into potato wedges, that is also an option.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1673716274/Blog-Assets%20TwT/Spanish%20Omelet/2_jckgns.jpg)

Cook them in oil. I won't go into specifics about the amount of oil because it depends on the pan you're using. Simply cook the potatoes. 1 medium onion, cut into small pieces, should be added to the pan where we are cooking the potatoes. As needed, season with salt.

I'm going to use 5 eggs for this recipe. We shall begin by thoroughly defeating them. Mix in 5 pinches of salt. In a separate pan, we'll add the fried potatoes and onion from the previous stage. Pour the egg mixture over that.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1673716274/Blog-Assets%20TwT/Spanish%20Omelet/3_uginh9.jpg)

Here, I recommend combining everything thoroughly as soon as possible since we need to let it for a while so it shapes up as a nice circular omelet; otherwise, if we keep mixing, it will turn out scrambled thingi, which we do not want :)

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1673716274/Blog-Assets%20TwT/Spanish%20Omelet/4_o1xtuw.jpg)

Allow it to cook for a few minutes over medium heat before flipping the omelet using the plate technique. I used this method because, as I previously stated, I don't want it messed up. When it comes to serving, all I need is to be in great shape.

Make a coriander topping and your TRADITIONAL SPANISH omelet (TORTILLA ESPANOLA) is ready to serve.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1673716274/Blog-Assets%20TwT/Spanish%20Omelet/6_m5mvpt.jpg)


What kind of oil should I use for Tortilla Espanola?

The finest option is olive oil. However, do not use the best and most expensive one available. Olive oil will suffice for cooking/frying.

However, everyone's taste differs, so if you want to try it, lower the cooking time somewhat. If you press down on the omelet with a spatula and it remains "wobbly," the eggs have not yet set. If it's firm, the omelet is done.

No. This recipe calls for only five simple ingredients, and replacing is not an option. It will lose its genuine flavor.

You can, however, add more eggs if desired (1 or 2 is plenty for the amount of potatoes).



## Sign Up and be our Partners in Taste
Please [Subscribe to our YouTube Channel]() if you liked this Beef Burger recipe Also Click the BELL icon so you can be the first one to get notification of Taste with Taj new video). Thank you for signing up. It means a lot to me.


#tastewithtaj  #Breakfast #eggs #How To

## Filming and Post Processing :

 [Explore with Taj](https://www.youtube.com/c/ExplorewithTaj/videos)

## Gallery pictures





