---
title: "Quick Dessert at Home Strawberry Crepe Rolls"
description: 
date: 2022-07-16T16:33:07+05:00
image: https://res.cloudinary.com/taste-with-taj/image/upload/v1657998944/Blog-Assets%20TwT/Strawberry%20Crepe%20Rolls/thumb_5_zprm4u.jpg

categories:
    - Desserts
    - 

tags:
    - 
math: 
license: 
hidden: false
comments: true
draft: false
url: "/Quick-Dessert-at-Home-Crep-Rolls/"
---

{{< youtube sF_nvjCm8SE  >}}

## Quick Dessert at Home Strawberry Crepe Rolls

Salam and much love to my family. If you're doing well, we're here to make you happy with some delicious simple dessert at home, Strawberry and Chocolate Crepe Rolls.

There's no better way to elevate a standard crepe than with these incredible Strawberry Crepes with a chocolate filling and fresh strawberries. They're simple to create and ideal for a special brunch or dessert.

Crepes are thin and flat French-style crepes that may be cooked and filled with sweet or savory contents. We like these with Nutella and strawberries or bananas inside.

## Ingredients for Quick Dessert at Home Strawberry Crepe Rolls:

1 Cup of ALL-PURPOSE FLOUR 

A Pinch of SALT    

2 EGGs  

1 ¼ Cup of MILK   

2 Tbsp + for cooking MELTED BUTTER  

CHOCOLATE SPREAD 

STRAWBERRIES 

1 Cup of SUGAR  

1 Tsp CINNAMON POWDER 

## How to Make This Dessert:

Crepes vary from American pancakes in that they do not include a rising agent (such as baking soda or baking powder), therefore they are wafer thin and delicate rather than thick and fluffy.

### Preparing the Crepe Batter:

To produce Crepe material, take a large blow or container and add a couple of eggs. Make sure we beat the eggs well since the more thoroughly we beat the eggs at this step, the more delicious and soft the crepes will be.

Continue mixing after adding 1 1/4 cup of milk. Now stir in 1 cup all-purpose flour, a sprinkle of salt, and 2 tablespoons melted butter.

### Cooking Crepes: 

After a few minutes of cooking on low medium heat, add 1/4 cup of Crepe Batter right immediately. I'm using a nonstick pan to make it easier to distribute the batter and create thin layers of crepe.

We don't need butter at this stage since it's already in the batter:).
Cook one side of the crepe, then flip and cook the other side. Do not overcook right now since we will after assembly.

### Rolls Assembly:

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1657998944/Blog-Assets%20TwT/Strawberry%20Crepe%20Rolls/0_gbjf6g.jpg)

This is a simple process in which we put Nutella or chocolate spread over a crepe. Strawberries, cut into tiny pieces, should be placed on top.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1657998944/Blog-Assets%20TwT/Strawberry%20Crepe%20Rolls/DSC_6321.00_01_37_21.Still004_a0fbet.jpg)


Roll it in half from the center, then flip it from both sides.

### Frying Crepes:

The last procedure is to melt butter in a nonstick skillet and begin making crepe rolls. This is not a deep fry; we only need to cook both sides till golden brown. We're nearly there.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1657998944/Blog-Assets%20TwT/Strawberry%20Crepe%20Rolls/DSC_6321.00_02_09_19.Still003_g9bbwt.jpg)

### Serving Quick Dessert at Home Strawberries Crepe Rolls:

1 cup sugar and 1 tablespoon cinnamon powder Put ready-made crepes in there and sprinkle with this. They're hot and ready to serve, inshallah.
I hope you loved this recipe.

The crepe batter may be prepared a day ahead and refrigerated. Cooked crepes will keep in the fridge for 2-3 days if firmly covered.

0:00 Intro
0:12 Ingredients
0:35 Crepe Batter Recipe
1:14 Fry Crepes 
1:45 Crepes Roll Assembling 
2:37 Final Product Shape
2:56 Other Videos

## Filming and Post Processing :

 [Explore with Taj](https://www.youtube.com/c/ExplorewithTaj/videos)

## Gallery pictures

 
![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1657998944/Blog-Assets%20TwT/Strawberry%20Crepe%20Rolls/thumb_6_mi3xgd.jpg)




