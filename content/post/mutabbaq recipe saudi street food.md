---
title: "Mutabbaq Recipe Saudi Street Food"
description: 
date: 2022-09-21T17:33:07+05:00
image: https://res.cloudinary.com/taste-with-taj/image/upload/v1663939535/Blog-Assets%20TwT/Mutabbaq%20/saudi_street_food_mutabbaq_recipe_o4xd5r.jpg
categories:
    - Chicken Recipes
    - 
    

tags:
    - 

math: 
license: 
hidden: false
comments: true
draft: false
url: "/Mutabbaq-Recipe-Saudi-Street-Food/"
---

{{< youtube v8E79TpRzYM  >}}

## Murtabak |  Mutabbaq Recipe Saudi Street Food  | Arabic Food Recipes

Greetings / Salam to my family, I hope you are all doing well. I'm not sure if you've ever tried the famous saudi street food dish mutabbaq. We're going to make one at home today.

Some call it Mutabbaq, while others call it Murtabak. This is essentially a delicious stuffed paratha with spicy minced meat, which is a popular Saudi street food snack. This delicious, filling snack has won my heart!

A delicious minced meat and egg filling in a paratha pocket. It's such a satisfying snack, and it's far superior to a plain paratha. Plain parathas are delicious, but I think we can all agree that stuffed parathas are even better. Especially if they have the appearance of cute pockets. I'm talking about delicious, crispy, golden pockets.

## Let's look at what we'll need.

½ Kg CHICKEN ( mince )

8 EGGs

1 Tbsp YOGURT

1 Tsp SALT

¼ Tsp WHITE PEPPER 

1 Tsp PAPRIKA POWDER 

½ Tsp BLACK PEPPER

1 Tbsp WHOLE WHEAT FLOUR 

3 GREEN ONIONS ( White Portion )

3 LARGE TOMATOES 

3 GREEN ONIONS ( Green Portion )

8 CHEESE TRIANGLES 

3 LEMONS 

2 Tbsp OIL for FRY 

2 CUPS of ALL PURPOSE FLOUR 

WATER

## Murtabak recipe | mutabbaq saudi street food instructions

fried square-shaped (baked in some regions) After cooking, a thin layer of bread is stuffed with minced meat (boiled with garlic), beaten eggs, chopped leeks, green onion, and cut into smaller squares before being served with lemon and green chilli.

Fun fact: because it is now made globally, its name has evolved from Murtabak, Martabak, Matabbak, Muttabak, Metabbak, Mutabbaq, and people refer to it as a spinoff of Mughlai Paratha.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1663939534/Blog-Assets%20TwT/Mutabbaq%20/DSC_3520.00_00_45_20.Still001_g98uxh.jpg)


### Making Mutabbaq Dough or Wrap

Add 2 cups of all-purpose flour and water to a mixing bowl and begin mixing with your hands. Continue kneading it until it forms dough.

Once the dough has been formed, cut it into equal-sized pieces, as shown in the picture.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1663939534/Blog-Assets%20TwT/Mutabbaq%20/DSC_3520.00_01_24_00.Still002_zw90cn.jpg)

Apply some oil to your palm and begin wrapping one piece out of four. Make dough balls like we do in the subcontinent for paratha or roti. Bingo 8 dough balls are ready; I'm ensuring that enough oil is applied for the next step. Cover and set aside for nearly 30 minutes.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1663939535/Blog-Assets%20TwT/Mutabbaq%20/DSC_3520.00_02_06_02.Still003_xp8ieu.jpg)

### Preparation of Meat Stuff:

In a nonstick pan, heat 2 tablespoons of oil and fry the white portion of 3 green onions. Before adding anything else, we need to fry them for a while. Once it starts to smell amazing, add 1/2 kg of thinly minced chicken. While on the stove, combine 1 tbsp yoghurt, 1 tsp salt, 1/4 tsp white pepper, 1/2 tsp black pepper, and 1 tsp paprika powder. Cover it with a plate or top cover.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1663939535/Blog-Assets%20TwT/Mutabbaq%20/DSC_3520.00_02_38_14.Still004_bey1ha.jpg)

Remove the plate, and trust me when you see brown cooked chicken, you will fall in love with this recipe. Yes, I am aware that there is a chicken odour at this point; however, adding lemon juice will not only eliminate the chicken odour but will also improve the taste.

So this is where mutabbaq becomes extremely simple to prepare. Instead of making your own dough and rolling it out thin, you can simply use spring roll wrappers. YES! This is exactly what I would do in my everyday life. I mean, I enjoy cooking shortcuts when I can, and believe me when I say this: it tastes just as good as mutabbaq made from scratch.

### Making Wraps from Chicken Material (Final Stage)

We have dough balls ready and will roll them out with a rolling pin to make sure the wrap is as thin as possible so that we can cook mutabbaq wraps.

Now we'll take some of the chicken material we made earlier (enough for 1 mutabbaq), add a green portion of 3 green onions, and large pieces of tomatoes. Finally, add a delicious Cheese Triangle to take the flavour of this dish to the next level. Mix thoroughly and add to the dough wrap.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1663939535/Blog-Assets%20TwT/Mutabbaq%20/DSC_3520.00_04_37_21.Still005_jctjjh.jpg)

We need to seal all sides of the wrap so that the egg-based chicken material stays inside while cooking. Cook the wrap from one side first, then flip it and cook the other side.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1663939535/Blog-Assets%20TwT/Mutabbaq%20/DSC_3520.00_04_58_21.Still006_ugnpiz.jpg)


Guess what our saudi street food mutabbaq recipe is ready to serve. Add some lemon drops for extra flavour.

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1663939535/Blog-Assets%20TwT/Mutabbaq%20/YT_DSC_3535_zwzah3.jpg)


## Some Vital Information

Rest the dough for at least 30 minutes, or prepare it the night before and store it in an airtight container in the fridge. Remove when preparing the meat filling.


Cook on medium high heat until the mutabbaq is well cooked from the inside and crisps up.

Mutabbaq can be kept in an airtight container in the fridge for up to 1-2 days. Place it on a baking tray in a preheated oven for about 15 minutes to reheat. It will heat up quickly but may not be as crispy as when fresh.


Green onion tip: I buy a large lot of green onions, slice them, and freeze them in a ziplock bag. Bring them out when you need them.

## Sign Up and be our Partners in Taste

Please [Subscribe to our YouTube Channel]() if you liked this Beef Burger recipe Also Click the BELL icon so you can be the first one to get notification of Taste with Taj new video). Thank you for signing up. It means a lot to me.


## Filming and Post Processing :

 [Explore with Taj](https://www.youtube.com/c/ExplorewithTaj/videos)

## Gallery pictures

 
![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1663939536/Blog-Assets%20TwT/Mutabbaq%20/saudi_street_food_mutabbaq_recipe_2_owyvib.jpg)






